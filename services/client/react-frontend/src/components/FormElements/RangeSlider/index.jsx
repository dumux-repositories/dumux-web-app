import React, { useState } from 'react'
import { Form } from 'react-bootstrap'
import InfoPopover from '../InfoPopover'
import './range-slider.css'

function RangeSlider({
  name,
  label,
  min,
  max,
  defaultValue,
  step,
  onChange,
  infoText,
  infoTitle,
}) {
  const [value, setValue] = useState(defaultValue ? defaultValue : min)

  const handleValueChanged = (e) => {
    setValue(e.target.value)
    onChange(name, e.target.value)
  }

  return (
    <Form.Group controlId={name}>
      <Form.Label
        style={{
          position: 'relative',
        }}
      >
        <span>{label}</span>
      </Form.Label>
      <div className='d-flex justify-content-center'>
        <input
          type='range'
          className='custom-range'
          id={name}
          min={min}
          max={max}
          defaultValue={defaultValue ? defaultValue : min}
          step={step ? step : 1}
          onInput={(e) => handleValueChanged(e)}
          onChange={(e) => handleValueChanged(e)}
        />
        <span className='range-output'>{value}</span>
      </div>
      {infoText ? (
        <InfoPopover text={infoText} title={infoTitle || label} key='tooltip' />
      ) : null}
    </Form.Group>
  )
}

export default RangeSlider
