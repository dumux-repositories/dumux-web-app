import React from 'react'
import { Base64 } from 'js-base64'

function FileDownloadViewer({ fileName, fileContent, mimetype, hidden }) {
  const content = Base64.decode(fileContent)
  const blob = new Blob([content], { type: mimetype })
  const file = new File([blob], fileName)
  const fileUrl = window.URL.createObjectURL(file)

  if(hidden || !fileUrl) {
    return null
  }

  return (
    <div>
      <a href={fileUrl} download={fileName}>{fileName}</a>
    </div>
  )
}

export default FileDownloadViewer
