import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import DynamicForm from '../../components/DynamicForm'
import LoadingDots from '../../components/LoadingDots'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Subnav from '../../components/Subnav'
import AboutSection from '../../components/AboutSection'
import ActionBar from '../../components/ActionBar'

function ConfigureSimulation({ match }) {
  const history = useHistory()
  // to hold configuration data
  const [configData, setConfigData] = useState(null)
  // the selected view
  const [selectedView, setSelectedView] = useState('about')
  // to hold a reference to the parameter input form
  const [parameterForm, setParameterForm] = useState(null)
  // get the simulation id from the url
  const simulationId = match.params.id
  // array of chart output configurations
  const [chartOutputConfig, setChartOutputConfig] = useState([])
  // array of vtk output configurations
  const [vtkOutputConfig, setVtkOutputConfig] = useState([])

  // the configuration data fetching effect
  useEffect(() => {
    // if we don't already have data and we have a simulation id
    if (!configData && simulationId) {
      // fetch the simulation configuration file
      fetch(
        process.env.REACT_APP_SERVER_URL
        + process.env.REACT_APP_API_BASE_URL
        + '/config?simulationId='
        + simulationId
      )
        .then((response) => response.json())
        .then((data) => {
          // set state with the config data
          setConfigData(data)
          if (data.configuration && data.configuration.output) {
            data.configuration.output.forEach((output) => {
              if (output.type === 'chart') {
                setChartOutputConfig((current) => [...current, output])
              } else if (output.type === 'vtk') {
                setVtkOutputConfig((current) => [...current, output])
              }
            })
          }
        })
        .catch((error) => {
          console.error('API Error:', error)
        })
    }
  }, [configData, simulationId])

  /**
   * On form submit setup a web socket and pass the form data
   */
  const handleFormSubmit = (request) => {
    // navigate to /run and pass data in state
    history.push({
      pathname: `/run/${simulationId}`,
      state: {
        request: request,
        configData: configData,
        chartOutputConfig: chartOutputConfig,
        vtkOutputConfig: vtkOutputConfig,
      },
    })
  }

  /**
   * Render loading screen if no config data
   */
  if (!configData) {
    return (
      <div id='app'>
        <Header title='' reducedHeight withBack />
        <Subnav />
        <div id='main-content'>
          <LoadingDots />
        </div>
        <Footer reducedHeight />
      </div>
    )
  }

  const handleSelect = (value) => {
    setSelectedView(value)
  }

  const menuItems = [
    {
      name: 'About',
      value: 'about',
    },
    {
      name: 'Set Parameters',
      value: 'parameters',
    },
  ]

  const onFormMounted = (ref) => {
    setParameterForm(ref)
  }

  return (
    <div id='app'>
      <Header title={configData.name} reducedHeight withBack />
      <Subnav
        menuItems={menuItems}
        onSelect={handleSelect}
        activeItem={selectedView}
      />
      <div id='main-content'>
        <div className='single-column'>
          <AboutSection
            description={configData.description}
            image_url={configData.image_url}
            hidden={selectedView !== 'about'}
          />
          <DynamicForm
            formFields={configData.formFields}
            onSubmit={handleFormSubmit}
            hidden={selectedView !== 'parameters'}
            onFormMounted={onFormMounted}
          />
        </div>
      </div>
      <ActionBar
        buttonText='Run Simulation'
        onClick={() => {
          parameterForm.current.dispatchEvent(new Event('submit', { cancelable: true }))
        }}
      />
      <Footer reducedHeight />
    </div>
  )
}

export default ConfigureSimulation
