import React from 'react'
import { Form } from 'react-bootstrap'
import InfoPopover from '../InfoPopover'
import './select.css'

function Select({
  name,
  label,
  defaultValue,
  options,
  onChange,
  titleOption,
  infoText,
  infoTitle,
}) {
  let optionMarkup = []

  if (titleOption) {
    optionMarkup.push(
      <option value='' key='39839'>
        {titleOption}
      </option>
    )
  }

  options.forEach((option, index) => {
    optionMarkup.push(
      <option value={option.value} key={index}>
        {option.label}
      </option>
    )
  })

  const renderLabel = () => {
    return <Form.Label>{label}</Form.Label>
  }

  return (
    <Form.Group controlId={name}>
      {label ? renderLabel() : null}
      <Form.Control
        as='select'
        defaultValue={defaultValue}
        onChange={(e) => onChange(name, e.target.value)}
      >
        {optionMarkup}
      </Form.Control>
      {infoText ? (
        <InfoPopover
          text={infoText}
          title={infoTitle || label}
          marginTop='-30px'
          key='tooltip'
        />
      ) : null}
    </Form.Group>
  )
}

export default Select
