import React, { useState } from 'react'
import { Modal } from 'react-bootstrap'
import { ReactComponent as QuestionIcon } from './question-icon.svg'
import './info-popover.css'

function InfoPopover({ text, title, marginTop }) {
  const [show, setShow] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  return (
    <>
      <div
        className={`icon-button small information-icon`}
        style={{
          marginTop: marginTop ? marginTop : '',
        }}
        onClick={handleShow}
      >
        <QuestionIcon />
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        // because of the findDomNode error thrown we have to disable animation here
        animation={false}
        style={{
          color: '#111111',
        }}
      >
        <Modal.Header
          closeButton
          style={{
            fontWeight: 'bold',
            borderBottom: 'none',
            paddingBottom: '0.5rem',
          }}
        >
          {title ? title : 'About'}
        </Modal.Header>
        <Modal.Body
          style={{
            paddingTop: '0px',
            paddingBottom: '1rem',
          }}
        >
          {text}
        </Modal.Body>
      </Modal>
    </>
  )
}

export default InfoPopover
