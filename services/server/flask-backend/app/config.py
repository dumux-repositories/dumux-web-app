import os
basedir = os.path.abspath(os.path.dirname(__file__))

class ConfigDevel(object):
    # a random string for security
    SECRET_KEY = os.environ.get('SECRET_KEY') or b'_5#y2L"F4Q8z\n\xec]/'
    # automaticall reload when refreshing site
    TEMPLATES_AUTO_RELOAD = True

class ConfigProduction(object):
    # a random string for security
    SECRET_KEY = os.environ.get('SECRET_KEY') or b'_5#y2L"F4Q8z\n\xec]/'

config = {
    'development': ConfigDevel,
    'production': ConfigProduction
}
