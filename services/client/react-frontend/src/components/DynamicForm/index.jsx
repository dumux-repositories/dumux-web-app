import React, { useRef, useEffect } from 'react'
import { Form } from 'react-bootstrap'
import Title from '../FormElements/Title'
import RangeSlider from '../FormElements/RangeSlider'
import Checkbox from '../FormElements/Checkbox'
import Select from '../FormElements/Select'

function DynamicForm({ formFields, onSubmit, hidden, onFormMounted }) {
  const form = useRef(null)
  let formData = []
  let markup = []

  if (formFields) {
    formFields.forEach((formField) => {
      if (formField.name) {
        let newFormGroup = {
          name: formField.name,
          value: formField.defaultValue ? formField.defaultValue : null,
        }

        formData.push(newFormGroup)
      }
    })
  }

  const handleChange = (name, value) => {
    formData.forEach((formElement) => {
      if (formElement.name === name) {
        formElement.value = value
      }
    })
  }

  if (formFields) {
    formFields.forEach((formField, index) => {
      switch (formField.type) {
        case 'title':
          markup.push(<Title text={formField.text} key={index} />)
          break
        case 'range':
          markup.push(
            <RangeSlider
              name={formField.name}
              label={formField.label}
              min={formField.min}
              max={formField.max}
              defaultValue={formField.defaultValue}
              step={formField.step}
              key={index}
              onChange={handleChange}
              infoText={formField.infoText}
              infoTitle={formField.infoTitle}
            />
          )
          break
        case 'select':
          markup.push(
            <Select
              name={formField.name}
              label={formField.label}
              defaultValue={formField.defaultValue}
              options={formField.options}
              onChange={handleChange}
              key={index}
              infoText={formField.infoText}
              infoTitle={formField.infoTitle}
            />
          )
          break
        case 'checkbox':
          markup.push(
            <Checkbox
              label={formField.label}
              name={formField.name}
              defaultValue={formField.defaultValue}
              onChange={handleChange}
              key={index}
              infoText={formField.infoText}
              infoTitle={formField.infoTitle}
            />
          )
          break
        default:
          console.log('Unrecognised form field:')
          console.log(formField)
      }
    })
  }

  useEffect(() => {
    onFormMounted(form)
  }, [onFormMounted])

  return (
    <Form
      id='simulationParameterForm'
      ref={form}
      className={hidden ? 'hidden' : ''}
      onSubmit={(e) => {
        e.preventDefault()

        let parsedFormData = {}

        formData.forEach((formGroup) => {
          let name = formGroup.name
          let value = formGroup.value
          parsedFormData[name] = value
        })

        onSubmit(parsedFormData)
      }}
    >
      {markup}
    </Form>
  )
}

export default DynamicForm
