import React from 'react'
import { Link } from 'react-router-dom'
import { ReactComponent as DumuxLogo } from '../../images/logo_white.svg'
import { ReactComponent as DumuxLogoReduced } from '../../images/logo_reduced.svg'
import { ReactComponent as CloseIcon } from './close-icon.svg'
import './header.css'

function Header({ title, reducedHeight, withBack }) {
  const renderTitle = () => {
    return (
      <h1 className='page-title'>{title}</h1>
    )
  }

  const renderBackButton = () => {
    return (
      <Link to='/' className='icon-button'>
        <CloseIcon icon='close' fill='#ffffff' width='14' />
      </Link>
    )
  }

  return (
    <header className={ reducedHeight ? 'reduced-height' : ''}>
      <div className='header-left'>
        <Link to='/' style={{ display: 'block' }}>
          { reducedHeight ? <DumuxLogoReduced height='24px'/> : <DumuxLogo height='36px'/>}
        </Link>
      </div>
      <div className='header-center'>{title ? renderTitle() : null}</div>
      <div className='header-right'>{withBack ? renderBackButton() : null}</div>
    </header>
  )
}

export default Header
