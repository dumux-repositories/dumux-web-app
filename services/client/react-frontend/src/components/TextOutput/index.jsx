import React, { useEffect, useRef } from 'react'
import LoadingDots from '../LoadingDots'
import TextOutputLine from './TextOutputLine'
import './text-output.css'

function TextOutput({ outputStream, loading, hidden }) {
  const outputArea = useRef(null)

  const makeKey = (prefix, index) => {
    return prefix + '-' + index.toString()
  }

  let markupLines = []
  if (outputStream) {
    markupLines = outputStream.map((item, index) => {
      return (
        <TextOutputLine line={item.line} lineType={item.lineType} lineNumber={index} key={makeKey('output-line', index)}/>
      )
    })
  }

  useEffect(() => {
    if (!hidden) {
      outputArea.current.scrollTop = outputArea.current.scrollHeight
    }
  })

  if (hidden) {
    return null
  }

  return (
    <div id='main-content' className='flex-column align-stretch'>
      <div id='output-area' className='form-control' ref={outputArea}>
        <code>
          <div>
            {markupLines.length > 0 && markupLines}
          </div>
          {loading && <LoadingDots />}
        </code>
      </div>
    </div>
  )
}

function makeInfoLine(message) {
  return {
    line: {
      text: message,
    },
    lineType: 'info',
  }
}

function makeResultLines(stdout) {
  const rawLines = stdout.split('\n')
  let lines = []
  rawLines.forEach(line => {
    if (line) {
      lines.push({
        line: {
          text: line,
        },
        lineType: 'result',
      })
    }
  })
  return lines
}

function makeLinkLine({ label, link }) {
  return {
    line: {
      text: label,
      link: link,
    },
    lineType: 'link',
  }
}

export default TextOutput
export { makeInfoLine, makeResultLines, makeLinkLine }
