FROM ubuntu:focal

# cleanup
RUN rm -f /etc/apt/apt.conf.d/docker-gzip-indexes && \
    rm -rf /var/lib/apt/lists/*

# install dependencies
RUN apt-get update && apt-get dist-upgrade --no-install-recommends --yes && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes \
    ca-certificates \
    ssh \
    python3 \
    python3-pip \
    python3-venv \
    docker \
    gunicorn \
    netbase \
    libmagic-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# expose the api port
EXPOSE 5000

# install Python requirements
COPY requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt

# copy all app files
COPY . /app

# switch to app directory
WORKDIR /app

# run the gunicorn server
CMD ["gunicorn", "-b", "0.0.0.0:5000", "--log-file=-", "--worker-tmp-dir", "/dev/shm", "--worker-class", "eventlet", "--workers", "1", "--threads", "2", "wsgi:application"]
