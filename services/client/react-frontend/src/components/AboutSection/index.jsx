import React, { useState } from 'react'
import LoadingDots from '../LoadingDots'
const commonmark = require('commonmark')

function AboutSection({ description, image_url, hidden }) {
  let imageURLGiven = image_url ? true : false
  const [loadingImage, setLoadingImage] = useState(imageURLGiven)

  const onImageLoaded = () => {
    setLoadingImage(false)
  }

  const renderDescription = () => {
    if (description) {
      const reader = new commonmark.Parser()
      const writer = new commonmark.HtmlRenderer()
      const parsed = reader.parse(description)
      return writer.render(parsed) // returns a string
    }
  }

  return (
    <div className={hidden ? 'hidden' : ''}>
      <div style={{ marginBottom: '24px' }} dangerouslySetInnerHTML={{ __html: renderDescription() }}></div>
      {image_url ? (
        <img
          src={image_url}
          width='100%'
          height='auto'
          alt={'Dumux simulation'}
          onLoad={onImageLoaded}
          className={loadingImage ? 'hidden' : ''}
        />
      ) : null}
      {loadingImage ? <LoadingDots /> : null}
    </div>
  )
}

export default AboutSection
