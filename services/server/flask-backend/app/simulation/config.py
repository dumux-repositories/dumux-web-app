# helper functions to handle the simulation configuration

# TODO implement a config validity checker
def checkConfig(config):
    pass

# apply some transformations before sending it to the frontend
def applyTransformations(config):

    try:
        description_file = config["description_file"]
        if "description" in config:
            print("Warning: Found config keys description_file and description. Overwriting description with file content!")
        with open("static/data/" + description_file, 'r') as description:
            config["description"] = description.read()

    except KeyError:
        pass

    return config
