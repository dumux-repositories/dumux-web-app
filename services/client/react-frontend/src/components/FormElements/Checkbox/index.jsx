import React from 'react'
import { Form } from 'react-bootstrap'
import InfoPopover from '../InfoPopover'

function Checkbox({
  name,
  defaultValue,
  label,
  onChange,
  infoText,
  infoTitle,
}) {
  return (
    <Form.Group controlId={name}>
      <Form.Check
        type='checkbox'
        label={label}
        defaultChecked={defaultValue}
        onChange={(e) => onChange(name, e.target.checked)}
      />
      {infoText ? (
        <InfoPopover text={infoText} title={infoTitle || label} key='tooltip' />
      ) : null}
    </Form.Group>
  )
}

export default Checkbox
