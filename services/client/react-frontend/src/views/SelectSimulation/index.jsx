import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import Footer from '../../components/Footer'
import Header from '../../components/Header'
import LoadingDots from '../../components/LoadingDots'
import Select from '../../components/FormElements/Select'

function SelectSimulation() {
  const History = useHistory()
  const [simulationConfig, setsimulationConfig] = useState(null)

  if (!simulationConfig) {
    fetch(
      process.env.REACT_APP_SERVER_URL
      + process.env.REACT_APP_API_BASE_URL
      + '/simulations'
    )
      .then((response) => response.json())
      .then((data) => {
        setsimulationConfig(data)
      })
      .catch((error) => {
        console.error('API Error:', error)
      })
  }

  const handleSelectChanged = (name, value) => {
    if (value) {
      // navigate to config url for the selected simulation
      History.push({
        pathname: `/config/${value}`,
      })
    }
  }

  const renderSelectDropdown = (app) => {
    return (
      <Select
        name='selectSimulation'
        options={app.cases}
        onChange={handleSelectChanged}
        titleOption={app.label || 'Select app'}
      />
    )
  }

  const renderDescription = () => {
    if (simulationConfig) {
      return (
        <>
          <h2>{simulationConfig.title || ''}</h2>
          <p>{simulationConfig.description || ''}</p>
        </>
      )
    }
  }

  return (
    <div id='app'>
      <Header />
      <div id='main-content' className='align-center'>
        <div className='single-column'>
          {simulationConfig ? renderDescription() : ''}
          {simulationConfig ? simulationConfig.applications.map((app, index) => {
            return (
              <div key={index}>
                <h3>{app.title || ''}</h3>
                <p>{app.description || ''}</p>
                <div className='form-group' id='selectContainer'>
                  {simulationConfig ? renderSelectDropdown(app) : <LoadingDots />}
                </div>
              </div>
            )
          }) : ''}
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default SelectSimulation
