import React from 'react'
import './action-bar.css'
import { Button } from 'react-bootstrap'

function ActionBar({buttonText, onClick}) {
  return (
    <div className='action-bar'>
      <div className='action-bar-inner'>
        <Button onClick={onClick}>{ buttonText }</Button>
      </div>
    </div>
  )
}

export default ActionBar
