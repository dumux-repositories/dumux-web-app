# Development setup

The following sections describe two local setups for development.
In the first one you manually start backend and frontend in development mode.
In the second setup both services are run in Docker containers.
Also, in the second setup the backend is served with a production-ready server.
For both types of setups you first need to pull the Docker images for the
DuMux demonstrator applications.

## Pull docker images for the demonstrator applications

You need to pull the Docker images for the demonstrator applications before
you can run the first test case through the web app.

```
docker pull git.iws.uni-stuttgart.de:4567/dumux-pub/berre2019a:latest
docker pull git.iws.uni-stuttgart.de:4567/dumux-appl/dumux-turing:latest
```

If these commands fail due to access restrictions, you might need to login with
your GitLab credentials (for the IWS Uni Stuttgart GitLab) first:

```
docker login git.iws.uni-stuttgart.de:4567
```

## Development setup without Docker (local setup)

### 1) Setup the flask backend (Python) for development

We recommend setting up a virtual environment as follows.
In the `services/server/flask-backend` folder run
```
python3 -m venv venv
source venv/bin/activate
```
to setup and active the virtual environment.

Install all dependencies by running
```
pip3 install -r requirements.txt
```

To run the app type
```
python3 -m flask run
```

The app will be served at `localhost:5000` but the user
will interact with the frontend which is set up as described below.

### 2) Pull docker images for the demonstrator applications (local development setup)

You need to pull the Docker images for the demonstrator applications before
you can run the first test case through the web app.

```
docker pull git.iws.uni-stuttgart.de:4567/dumux-pub/berre2019a:latest
docker pull git.iws.uni-stuttgart.de:4567/dumux-appl/dumux-turing:latest
```

If these commands fail due to access restrictions, you might need to login with
your GitLab credentials (for the IWS Uni Stuttgart GitLab) first:

```
docker login git.iws.uni-stuttgart.de:4567
```


### 3) Setup the react frontend (JavaScript) for development

The application frontend is built with `react.js`

To setup for development, in the `services/client/react-frontend` folder run:
```
npm install
```

Then to start the node process:
```
npm start
```

The development server starts on `localhost:3000`.
If the flask server is running too, the front end can now be developed.

## Development setup (local setup) with Docker

This section describes how to setup a development environment where both
frontend and backend run in Docker containers. The backend is served by
the Python server application `gunicorn`. The backend actually runs in
a production-ready mode while the frontend can be developed.

### 1) Build Docker images for all services

In the topmost folder of the repository run

```
docker-compose -f docker-compose-dev.yml build
```

This command builds two Docker images.
One for the frontend and one for the backend.

### 2) Start dockerized web services

In the topmost folder of the repository run

```
docker-compose -f docker-compose-dev.yml up
```

The web-app will be served on `localhost:3000`.
You can now start editing the fronted files to develop the app
and see it updating live.

# Test production setup with Docker

This section describes how to setup a test production environment where both
frontend and backend run in Docker containers. The backend is served by
the Python server application `gunicorn`. `redis` is used as a message queue.
The frontend is built with `npm`.
Backend, frontend and static files are served by an `nginx` server in reverse proxy mode.

### 1) Build Docker images for all services

In the topmost folder of the repository run

```
docker-compose build
```

This command builds two Docker images.
One for the frontend and one for the backend.

### 2) Start dockerized web services

In the topmost folder of the repository run

```
docker-compose up
```

This will start the frontend, backend and redis services.
You can now test the production setup locally.
The web-app will be served on `localhost:3000`.

### Deployment at subdirectory (e.g. dumux.org/dumux-web-app)

For the frontend: Add the entry
```
"homepage": "/dumux-web-app"
```
in the `package.json` file.

For the backend: Add the sub-directory route as
environment variable `FLASK_APP_BASE_URL=/dumux-web-app`
and make the frontend know about the new api route
by setting `REACT_APP_API_BASE_URL=/dumux-web-app/api`.
Make sure all assets (like images) use relative paths
(see `simulation-2.json` as an example).

# Production setup (server setup)

## 1) Setup the react frontend for production

Note: This section is incomplete. TODO: Describe production setup with nginx.

To build the application run:
```
npm run build
```

Note: for a production build the file `services/client/react-frontend/.env.production`
will need to be edited to set the correct URLs for HTTP requests.
