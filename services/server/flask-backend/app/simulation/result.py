import time
import datetime
import magic
import os
import re
import url64
import uuid

from app.extensions import socketio
from threading import Thread

# streamer class handling the streaming of the simulation output back to the app
class ResultStreamer(Thread):
    def __init__(self, stream, directory, clientId, ignored_files=[], output_keys=[], message_interval=0.5):
        super(ResultStreamer, self).__init__()
        self.stream = stream
        self.sent_files = []
        self.ignored_files = ignored_files
        self.output_keys = output_keys
        self.directory = directory
        self.message_interval = message_interval
        self.clientId = clientId

    def run(self):
        stdout_chunk, stderr_chunk = "", ""
        start_time, chunk_time = time.time(), 0
        for stdout, stderr in self.stream:
            if stdout is not None:
                stdout_chunk += stdout.decode('utf-8')
            if stderr is not None:
                stderr_chunk += stderr.decode('utf-8')
            chunk_time += time.time() - start_time
            if chunk_time > self.message_interval:
                if stdout_chunk or stderr_chunk:
                    self.create_result(stdout=stdout_chunk, stderr=stderr_chunk, status="intermediate")
                    stdout_chunk, stderr_chunk = "", ""
                    start_time, chunk_time = time.time(), 0

        # the container has finished and we can create the final results
        self.create_result(stdout=stdout_chunk, stderr=stderr_chunk, status="final")

    # create a result message in the right format
    def create_result(self, stdout, stderr, status):
        print("Create result event, status: {}".format(status))
        result = {"computation": "some-id-dkanvasindoasindoaisdn",
                  "status": status,
                  "timestamp": datetime.datetime.now().isoformat(),
                  "notifications": {},
                  "output": {"stdout": url64.encode(stdout),
                             "stderr": url64.encode(stderr)},
                  "files": []}

        def attach_files(output_files):
            if output_files:
                output_files = sorted(output_files)
                mime = magic.Magic(mime=True)
                for file_name in output_files:
                    mimetype = mime.from_file(file_name)
                    with open(file_name, 'rb') as file_handle:
                        result["files"].append({
                            "identifier": str(uuid.uuid4()),
                            "path": file_name,
                            "name": os.path.basename(file_name),
                            "mimetype": mimetype,
                            "content": url64.encode(file_handle.read())
                        })
                    self.sent_files.append(file_name)

        # if output files can be identified by a pattern match in
        # the stdout send them to the frontend
        output_files = self.find_output_files(stdout=stdout)
        attach_files(output_files)

        # if this is the final message send all files that have been
        # produced but haven't been sent yet
        # if status == "final":
        #     output_files = [os.path.join(r, file) for r, d, f in os.walk(self.directory) for file in f]
        #     output_files = [f for f in output_files if all([f not in self.sent_files, f not in self.ignored_files])]
        #     attach_files(output_files)

        # trigger result event
        socketio.emit('simulation-result', result, room=self.clientId)

    # find output files whose creating is documented in stdout and
    # matches a given pattern
    def find_output_files(self, stdout):
        output_files = []
        for key in self.output_keys:
            matches = re.findall(key.replace('[','\[').replace(']','\]')+'(.+?)\n', stdout)
            for match in matches:
                output_files.append(os.path.join(self.directory, match.strip()))

        return output_files
