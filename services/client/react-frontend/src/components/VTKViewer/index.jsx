import React, { useEffect, useState, useRef } from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faPlay,
  faPause,
  faForward,
  faFastForward,
  faBackward,
  faFastBackward,
  faDownload,
} from '@fortawesome/free-solid-svg-icons'
import Select from '../FormElements/Select'
import RangeSlider from '../FormElements/RangeSlider'
import './vtk-viewer.css'

// vtk basics
import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor'
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper'
import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction'

// data
import {
  ColorMode,
  ScalarMode,
} from 'vtk.js/Sources/Rendering/Core/Mapper/Constants'
import vtkColorMaps from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction/ColorMaps'

// IO
import vtkITKPolyDataReader from 'vtk.js/Sources/IO/Misc/ITKPolyDataReader'
import readPolyDataArrayBuffer from 'itk/readPolyDataArrayBuffer'

// render window
import vtkFullScreenRenderWindow from 'vtk.js/Sources/Rendering/Misc/FullScreenRenderWindow'

function VTKViewer({ files, hidden }) {
  const viewerContainer = useRef(null)
  const viewerContent = useRef(null)

  const [fileIndex, setFileIndex] = useState(0)
  const [reader, setReader] = useState(null)
  const [fullScreenRenderer, setFullScreenRenderer] = useState(null)
  const [pipeline, setPipeline] = useState(null)

  // view state and onChange handlers
  const [enableAutoPlay, setEnableAutoPlay] = useState(false)
  const [representationOnChange, setRepresentationOnChange] = useState(null)
  const [representation, setRepresentation] = useState('1:2:0')

  const [opacityOnChange, setOpacityOnChange] = useState(null)
  const [opacity, setOpacity] = useState(1)

  const [colorByOnChange, setColorByOnChange] = useState(null)
  const [colorBy, setColorBy] = useState(':Solid color')
  const [colorByOptions, setColorByOptions] = useState([
    {
      value: ':',
      label: 'Solid color',
    },
  ])

  // initialize buffer for the ITK reader
  vtkITKPolyDataReader.setReadPolyDataArrayBufferFromITK(
    readPolyDataArrayBuffer
  )

  const readRawData = ({ fileName, data }) => {
    return new Promise((resolve, reject) => {
      const sourceType = null
      const reader = vtkITKPolyDataReader.newInstance()
      reader.setFileName(fileName)
      try {
        const ds = reader.parseAsArrayBuffer(data)
        Promise.resolve(ds)
          .then((dataset) =>
            resolve({ dataset, reader, sourceType, name: fileName })
          )
          .catch(reject)
      } catch (e) {
        reject(e)
      }
    })
  }

  const readFile = (file) => {
    return new Promise((resolve, reject) => {
      const io = new FileReader()
      io.onload = function onLoad() {
        readRawData({ fileName: file.name, data: io.result })
          .then((result) => resolve(result))
          .catch((error) => reject(error))
      }
      io.readAsArrayBuffer(file)
    })
  }

  const fileToReader = async (file) => {
    const data = await readFile(file)
    setReader(data.reader)
  }

  // send current file to the reader
  useEffect(() => {
    if (files && files.length > 0 && reader) {
      console.log('Send file to reader: ', fileIndex + 1)
      fileToReader(files[fileIndex])
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fileIndex])

  // send first file to the reader
  useEffect(() => {
    if (files && files.length > 0 && !reader) {
      fileToReader(files[0])
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [files.length])

  // setup the render window & renderer
  useEffect(() => {
    if (!hidden) {
      console.log('Create render window')
      setFullScreenRenderer(
        vtkFullScreenRenderWindow.newInstance({
          background: [0.4, 0.44, 0.52],
          rootContainer: viewerContainer.current,
          container: viewerContent.current,
        })
      )

      const actor = vtkActor.newInstance()
      const lookupTable = vtkColorTransferFunction.newInstance()
      const mapper = vtkMapper.newInstance({
        interpolateScalarsBeforeMapping: false,
        useLookupTableScalarRange: true,
        lookupTable,
        scalarVisibility: false,
      })

      actor.setMapper(mapper)

      setPipeline({
        actor,
        lookupTable,
        mapper,
      })
    }
  }, [hidden])

  // render the pipeline if there is an active reader
  useEffect(() => {
    if (reader && fullScreenRenderer && pipeline) {
      console.log('Render pipeline for file: ', fileIndex + 1)

      const lookupTable = pipeline.lookupTable
      const actor = pipeline.actor
      const mapper = pipeline.mapper

      // get fields
      const source = reader.getOutputData()
      const scalars = source.getPointData().getScalars()
      const dataRange = [].concat(scalars ? scalars.getRange() : [0, 1])

      const applyPreset = () => {
        // TODO allow to choose from different presets
        const defaultLutName = 'erdc_rainbow_bright'
        const preset = vtkColorMaps.getPresetByName(defaultLutName)
        lookupTable.applyColorMap(preset)
        lookupTable.setMappingRange(dataRange[0], dataRange[1])
        lookupTable.updateRange()
      }

      setColorByOptions(
        [{ value: ':', label: 'Solid color' }].concat(
          source
            .getPointData()
            .getArrays()
            .map((a) => ({
              label: `(p) ${a.getName()}`,
              value: `PointData:${a.getName()}`,
            })),
          source
            .getCellData()
            .getArrays()
            .map((a) => ({
              label: `(c) ${a.getName()}`,
              value: `CellData:${a.getName()}`,
            }))
        )
      )

      const getMapperConfig = (value) => {
        const [location, colorByArrayName] = value.split(':')
        const interpolateScalarsBeforeMapping = location === 'PointData'
        let colorMode = ColorMode.DEFAULT
        let scalarMode = ScalarMode.DEFAULT
        const scalarVisibility = location.length > 0
        if (scalarVisibility) {
          const dataArray = source[`get${location}`]().getArrayByName(
            colorByArrayName
          )
          const newDataRange = dataArray.getRange()
          dataRange[0] = newDataRange[0]
          dataRange[1] = newDataRange[1]
          colorMode = ColorMode.MAP_SCALARS
          scalarMode =
            location === 'PointData'
              ? ScalarMode.USE_POINT_FIELD_DATA
              : ScalarMode.USE_CELL_FIELD_DATA
        }

        return {
          colorByArrayName,
          colorMode,
          interpolateScalarsBeforeMapping,
          scalarMode,
          scalarVisibility,
        }
      }

      // apply functions
      const applyOpacity = (actor, opacity) => {
        actor.getProperty().setOpacity(opacity)
      }

      const applyRepresentation = (actor, representation) => {
        const [visibility, repr, edgeVisibility] = representation
          .split(':')
          .map(Number)
        actor.getProperty().setRepresentation(repr)
        actor.getProperty().setEdgeVisibility(edgeVisibility)
        actor.setVisibility(!!visibility)
      }

      const applyColorBy = (actor, colorBy) => {
        const mapperConfig = getMapperConfig(colorBy)
        mapper.set(mapperConfig)
        applyPreset()
      }

      // set current configurations
      applyOpacity(actor, opacity)
      applyRepresentation(actor, representation)
      applyColorBy(actor, colorBy)

      // pipeline
      const renderer = fullScreenRenderer.getRenderer()
      const renderWindow = fullScreenRenderer.getRenderWindow()
      mapper.setInputConnection(reader.getOutputPort())
      renderer.removeAllActors()
      renderer.addActor(actor)
      renderer.resetCamera()
      renderWindow.render()
      console.log('-> Pipeline done.')

      // set onchange hooks
      const representationOnChange = (name, value) => {
        setRepresentation(value)
        applyRepresentation(actor, value)
        renderWindow.render()
      }

      setRepresentationOnChange(() => representationOnChange)

      const opacitySliderOnChange = (name, value) => {
        const opacity = Number(value) / 100
        setOpacity(opacity)
        applyOpacity(actor, opacity)
        renderWindow.render()
      }

      setOpacityOnChange(() => opacitySliderOnChange)

      const handleColorByOnChange = (name, value) => {
        setColorBy(value)
        applyColorBy(actor, value)
        renderWindow.render()
      }

      setColorByOnChange(() => handleColorByOnChange)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fullScreenRenderer, reader, pipeline, colorBy, opacity, representation])

  const renderViewController = () => {
    // first value is visibility of the model 0: off, 1: on
    // second value is the representation type 0: Points, 1: Wireframe, 2: Surface
    // (see also Representation in vtk.js/Sources/Rendering/Core/Property/Constants)
    // third value is the edgeVisibility (for Surface with Edges)
    const representationOptions = [
      {
        label: 'Wireframe',
        value: '1:1:0',
      },
      {
        label: 'Surface',
        value: '1:2:0',
      },
      {
        label: 'Surface with Edges',
        value: '1:2:1',
      },
    ]

    return (
      <div className='vtk-geometry-controls'>
        <div className='geometry-control-group'>
          <Select
            name='select-geometry'
            defaultValue='1:2:0'
            options={representationOptions}
            onChange={
              representationOnChange ? representationOnChange : () => {}
            }
          />
        </div>
        <div className='geometry-control-group with-padding'>
          <RangeSlider
            name='opacitySelector'
            label='Opacity'
            min={0}
            max={100}
            defaultValue={100}
            step={1}
            onChange={opacityOnChange ? opacityOnChange : () => {}}
          />
        </div>
        <div className='geometry-control-group'>
          <Select
            name='select-color'
            options={colorByOptions}
            onChange={colorByOnChange ? colorByOnChange : () => {}}
          />
        </div>
      </div>
    )
  }

  const decreaseFileIndex = () => {
    setFileIndex((index) => {
      return Math.max(index - 1, 0)
    })
  }

  const increaseFileIndex = () => {
    setFileIndex((index) => {
      return Math.min(index + 1, files.length - 1)
    })
  }

  // autoplay and pause
  useEffect(() => {
    if (enableAutoPlay) {
      const interval = setInterval(() => {
        increaseFileIndex()
      }, 1000)
      return () => clearInterval(interval)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [enableAutoPlay, fileIndex])

  const renderFileController = () => {
    return (
      <div className='file-controller text-center'>
        <div className='fixed-row-70 display-flex-center'>
          <span>
            {fileIndex + 1}/{files.length}
          </span>
        </div>
        <ButtonGroup>
          <Button variant='white-icon' onClick={() => setFileIndex(0)}>
            <FontAwesomeIcon icon={faFastBackward} />
          </Button>
          <Button variant='white-icon' onClick={decreaseFileIndex}>
            <FontAwesomeIcon icon={faBackward} />
          </Button>
          <Button
            variant='white-icon'
            onClick={() => setEnableAutoPlay((enabled) => !enabled)}
          >
            <FontAwesomeIcon icon={enableAutoPlay ? faPause : faPlay} />
          </Button>
          <Button variant='white-icon' onClick={increaseFileIndex}>
            <FontAwesomeIcon icon={faForward} />
          </Button>
          <Button
            variant='white-icon'
            onClick={() => setFileIndex(files.length - 1)}
          >
            <FontAwesomeIcon icon={faFastForward} />
          </Button>
        </ButtonGroup>
        <div className='fixed-row-70 display-flex-center'>
          <Button
            variant='white-icon'
            href={URL.createObjectURL(files[fileIndex])}
          >
            <FontAwesomeIcon icon={faDownload} />
          </Button>
        </div>
      </div>
    )
  }

  let windowHeight = window.innerHeight
  let viewerHeight = windowHeight - 66 - 48 - 66 - 96

  if (hidden) {
    return null
  }

  return (
    <div id='vtk-content'>
      {renderViewController()}
      <div className='viewer-container'>
        <div ref={viewerContainer} className={'viewer'}>
          <div
            ref={viewerContent}
            className='viewer-content'
            style={{ height: viewerHeight + 'px' }}
          ></div>
        </div>
      </div>
      {renderFileController()}
    </div>
  )
}

export default VTKViewer
