#!/bin/bash 

set -eu  # exit on error
set -x   # show commands

# not implemented is to have backup/restore of the previous installation on fail of setup
rm -rf continuous-deployment || true

git clone https://git.iws.uni-stuttgart.de/dumux-appl/dumux-web-app.git continuous-deployment
cd continuous-deployment 

# checkout a certain version

(($# >= 1)) && git checkout $1


# make the setup 

cd services/server/flask-backend
# pin Python version to 3.9
python3.9 -m venv venv
source venv/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
pip install gunicorn

# emit gunicorn-script used by systemd script 

cat <<- EOF > gunicorn.cmdline.sh 
	#!/bin/bash 

	source ./venv/bin/activate

	export FLASK_ENV=production
	export FLASK_APP_BASE_URL=/web-app
	export FLASK_APP_SIMULATIONS_CONFIG=simulations.json

	# have log file and stdout for journald 
	gunicorn -b "0.0.0.0:5000"   --proxy-allow-from="*"  --log-level debug --log-file=- --worker-tmp-dir /dev/shm --worker-class eventlet --workers 1 --threads 2 wsgi:application |& tee gunicorn.log 

EOF

chmod +x gunicorn.cmdline.sh

# restart the service 
sudo /bin/systemctl restart dumux-web-app
