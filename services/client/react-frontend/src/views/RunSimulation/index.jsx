import React, { useState, useEffect } from 'react'
import { useLocation, useHistory, useRouteMatch } from 'react-router-dom'
import TextOutput, {
  makeInfoLine,
  makeResultLines,
} from '../../components/TextOutput'
import LoadingDots from '../../components/LoadingDots'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import Subnav from '../../components/Subnav'
import ActionBar from '../../components/ActionBar'
import LineChart from '../../components/LineChart'
import VTKViewer from '../../components/VTKViewer'
import io from 'socket.io-client'
import { Base64 } from 'js-base64'

function RunSimulation() {
  const history = useHistory()
  const match = useRouteMatch()
  const location = useLocation()
  // the selected view
  const [selectedView, setSelectedView] = useState('stdOutput')
  // to notify when simulation data is loading
  const [simulationLoading, setSimulationLoading] = useState(false)
  // to hold result standard text output
  const [textOutputStream, setTextOutputStream] = useState([])
  // to hold result file output
  const [resultVTKFiles, setResultVTKFiles] = useState([])
  // to hold chart output
  const [chartOutput, setChartOutput] = useState([])

  const simulationId = match.params.id

  if (
    !location.state ||
    !location.state.configData ||
    !location.state.request
  ) {
    history.push({
      pathname: `/config/${simulationId}`,
    })
  }

  const configData = location.state.configData
  const simulationRequest = location.state.request
  const chartOutputConfig = location.state.chartOutputConfig
  const vtkOutputConfig = location.state.vtkOutputConfig

  // run simulation effect
  useEffect(() => {
    // create the socket and attach event handlers
    const socket = io(process.env.REACT_APP_SERVER_URL)

    // when the socket opens
    socket.on('connect', () => {
      console.log("New client connected " + socket.id)

      // create the 'create-computation' message
      const message = {
        type: 'create-computation',
        content: {
          parameters: simulationRequest,
          configuration: configData.configuration,
        },
      }

      // emit a 'run-simulation' event
      socket.emit('run-simulation', message)
      console.log("Emitted event 'run-simulation'")
      setSimulationLoading(true)
      setTextOutputStream([makeInfoLine('Simulation started.')])
      setSelectedView('stdOutput')
    })

    socket.on('disconnect', () => {
      console.log("Client disconnected.")
    })

    socket.on('simulation-start', (message) => {
      console.log("Simulation started.")
    })

    const createURLFromFile = (content, mimetype) => {
      return URL.createObjectURL(new Blob([content], { mimetype: mimetype }))
    }

    // when the event 'simulation-result' happens
    socket.on('simulation-result', (message) => {
      // update the text output items
      setTextOutputStream((items) => {
        let newItems = []
        if (message.output.stdout.length) {
          newItems.push(
            ...makeResultLines(Base64.decode(message.output.stdout))
          )
        }

        if (message.output.stderr.length) {
          newItems.push(
            ...makeResultLines(Base64.decode(message.output.stderr))
          )
        }

        if (message.files.length) {
          message.files.forEach((file) => {
            newItems.push(makeInfoLine('Received output file: ' + file.name))
          })
        }

        if (message.status === 'final') {
          newItems.push(makeInfoLine('Simulation done.'))
        }

        return [...items, ...newItems]
      })

      // handle file-based output modes
      if (message.files) {
        message.files.forEach((file) => {
          if (file.mimetype === 'application/csv') {
            // loop through the expected output configs
            chartOutputConfig.forEach((expectedOutput) => {
              // if there is a match with the base name and file name
              if (file.name.match(expectedOutput.basename)) {
                // update the matching chart output streams
                setChartOutput((currentOutput) => {
                  // look for the chart output stream with the right id
                  const stream = currentOutput.find(
                    (output) => output.id === expectedOutput.id
                  )
                  if (stream) {
                    stream.fileContent = Base64.decode(file.content)
                    stream.fileURL = createURLFromFile(
                      stream.fileContent,
                      file.mimetype
                    )
                    // turning this into a copy (e.g. slice) kills performance
                    return currentOutput
                  }
                  // if there is no existing stream for the data add a new one
                  else {
                    const stream = expectedOutput
                    stream.fileContent = Base64.decode(file.content)
                    stream.fileURL = createURLFromFile(
                      stream.fileContent,
                      file.mimetype
                    )
                    return [...currentOutput, stream]
                  }
                })
              }
            })
          } else if (file.mimetype === 'text/xml') {
            // loop through the expected output configs
            vtkOutputConfig.forEach((expectedOutput) => {
              // if there is a match with the base name and file name
              if (file.name.match(expectedOutput.basename)) {
                const content = Base64.decode(file.content)
                const blob = new Blob([content], { type: file.mimetype })
                const vtkFile = new File([blob], file.name)
                setResultVTKFiles((vtkFiles) => [...vtkFiles, vtkFile])
              }
            })
          }
        })
      }

      // simulation is done and doesn't need to load data anymore
      if (message.status === 'final') {
        setSimulationLoading(false)
      }
    })

    socket.on('simulation-end', (message) => {
      console.log("Simulation finished.")
      socket.disconnect()
    })

    return () => socket.disconnect()
  }, [configData, simulationRequest, chartOutputConfig, vtkOutputConfig])

  /**
   * On reset clear any result data
   */
  const handleReset = () => {
    setTextOutputStream([])
    setResultVTKFiles([])
    setChartOutput([])
    history.push({
      pathname: `/config/${simulationId}`,
    })
  }

  /**
   * Render loading screen if no config data
   */
  if (!configData) {
    return (
      <div id='app'>
        <Header title='' reducedHeight withBack />
        <Subnav />
        <div id='main-content'>
          <LoadingDots />
        </div>
        <Footer reducedHeight />
      </div>
    )
  }

  const handleSelect = (value) => {
    setSelectedView(value)
  }

  const resultMenuItems = [
    {
      name: 'Output',
      value: 'stdOutput',
    },
  ]

  if (resultVTKFiles.length > 0) {
    resultMenuItems.push({
      name: 'Visualisation',
      value: 'vtuOutput',
    })
  }

  if (chartOutput.length > 0) {
    resultMenuItems.push({
      name: 'Chart',
      value: 'chartOutput',
    })
  }

  let extraChartContainerClass = ''
  if(chartOutput && chartOutput.length > 0) {
    if(chartOutput.length === 1) {
      extraChartContainerClass = 'single-chart'
    } else if(chartOutput && chartOutput.length === 2) {
      extraChartContainerClass = 'double-charts'
    } else {
      extraChartContainerClass = 'multiple-charts'
    }
  }

  return (
    <div id='app'>
      <Header title={configData.name} reducedHeight withBack />
      <Subnav
        menuItems={resultMenuItems}
        onSelect={handleSelect}
        activeItem={selectedView}
      />
      <TextOutput
        outputStream={textOutputStream}
        loading={simulationLoading}
        hidden={selectedView !== 'stdOutput'}
      />
      {resultVTKFiles.length > 0 ? (
        <VTKViewer
          files={resultVTKFiles}
          hidden={selectedView !== 'vtuOutput'}
        />
      ) : null}
      {selectedView === 'chartOutput' && (
        <div id='main-content'>
          <div className={`chart-container ${extraChartContainerClass}`}>
            {chartOutput.map((outputGroup, index) => {
              return (
                <LineChart
                  fileContent={outputGroup.fileContent}
                  fileURL={outputGroup.fileURL}
                  labels={outputGroup.labels}
                  datasets={outputGroup.datasets}
                  title={outputGroup.title}
                  key={index}
                />
              )
            })}
          </div>
        </div>
      )}
      <ActionBar buttonText='Reset' onClick={handleReset} />
      <Footer reducedHeight />
    </div>
  )
}

export default RunSimulation
