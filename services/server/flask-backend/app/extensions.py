import os

env = os.environ.get('FLASK_ENV', 'development')

# socket multithreading event support (production only)
if env != 'development':
    import eventlet
    eventlet.monkey_patch()

from flask_bootstrap import Bootstrap
from flask_socketio import SocketIO
from flask_cors import CORS

# the web socket extension
socketio = SocketIO()
socketioConfig = {
    'cors_allowed_origins': '*'
}

if env != 'development':
    socketioConfig['async_mode'] = 'eventlet'
    socketioConfig['message_queue'] = 'redis://redis:6379'

# decorate this app with Bootstrap (for the good looks)
bootstrap = Bootstrap()

# enable Cross Origin Resource Sharing for the app
cors = CORS()

# make list of extensions importable
extensions = [
    (socketio, socketioConfig),
    bootstrap,
    cors
]
