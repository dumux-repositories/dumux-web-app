from flask import Flask
from app.config import config as configurations
from app.utils import registerExtensions
from app.routes import routes
import os

def create_app(configType=None):
    app = Flask(__name__)

    # config
    if configType is None:
        configType = os.environ.get('FLASK_CONFIG', 'development')

    config = configurations[configType]
    app.config.from_object(config)

    # register extensions
    from app.extensions import extensions as e
    registerExtensions(app, e)

    # register routes / views
    url_prefix = os.environ.get('FLASK_APP_BASE_URL', '/')
    app.register_blueprint(routes, url_prefix=url_prefix)

    return app
