from flask import request
from flask_socketio import emit
from app.simulation.result import ResultStreamer
import tempfile
import docker
import os

# this runs the actual simulation given a parameter file
# in which all the variables/placeholders have been replaced
# by the content specified in the UI
def run_simulation(message):
    # tell the front-end that the simulation started
    emit('simulation-start')

    # logging
    print("Start new simulation. Emitted event 'simulation-start'")

    # create a temporary directory and create the input parameter file
    tmp_dir = tempfile.TemporaryDirectory(prefix="dumuxapp_", dir="/tmp")

    # In the production environment which runs with userns-remap feature:
    # The user running the server must be in the group "containermnt"
    # and this group id must be identical to group id of the future running
    # process within the container. Not very perfect to mount when userns-remap
    # docker option is used.

    if os.environ.get('FLASK_ENV', 'development') == 'production':
        # effectivly do equivalent of
        # subprocess.run(['/usr/bin/chgrp', 'containermnt', tmp_dir.name], check=True)
        # + chmod
        # TODO: group data (groupname or gid) should come from a deployers config-file.
        import grp
        try:
           gid = grp.getgrnam('containermnt').gr_gid
           os.chown(tmp_dir.name, -1, gid)
           os.chmod(tmp_dir.name, 0o2770)
        except KeyError: # pass KeyError, which will likely happen in docker-compose
           print("No group named 'containermnt' found. This might lead to permissios",
                 "problems for running a container with userns-remap for the bind mount.")
           pass

    input_files = []
    for fileinfo in message["content"]["task"]["files"]:
        path = os.path.join(tmp_dir.name, fileinfo["path"])
        with open(path, 'w') as file:
            # TODO: multiple parts
            file.write(fileinfo["parts"])
        input_files.append(path)

    # TODO check the configuration data for mandatory fields
    config = message["content"]["configuration"]

    # watch for output files
    output = config.get("output", None)
    output_keys = []
    if output is not None:
        for outputElement in output:
            if "update" in outputElement:
                output_keys.append(outputElement["update"]["key"])

    # make output keys unique
    output_keys = list(set(output_keys))

    # start the docker client
    docker_client = docker.from_env()

    # load / pull the docker image
    docker_image_id = config["resources"]["image"]
    docker_image = docker_client.images.list(docker_image_id)
    if len(docker_image) == 0:
        docker_client.images.pull(docker_image_id)

    # logging
    print("Docker image found (id: '{}').".format(docker_image_id))

    # create docker container
    # the temporary directory is mounted as a volume
    # this volume is used to share data, for example, after the simulation
    # has finished, the ParaView visualization files will be in that folder
    container = docker_client.containers.create(docker_image_id, auto_remove=False, detach=True,
                                                entrypoint=config["running"].get("entrypoint", None),
                                                command=config["running"].get("commandLineArguments", "params.input"),
                                                mem_limit=config["resources"].get("memory", "1g"),
                                                volumes={tmp_dir.name: {"bind":config["resources"].get("volume", "/data/shared"), "mode":"rw"}})

    # logging
    print("Created Docker new container.")

    # attach log listener to the docker container to get output
    response_stream = container.attach(stdout=True, stderr=True, stream=True, logs=True, demux=True)
    result_stream_thread = ResultStreamer(stream=response_stream,
                                          directory=tmp_dir.name,
                                          ignored_files=input_files,
                                          output_keys=output_keys,
                                          clientId=request.sid)
    result_stream_thread.start()

    # start the container
    container.start()

    # wait for the simulation to be done
    result_stream_thread.join()

    # remove the container
    container.remove()

    # tell the front-end that the simulation ended
    emit('simulation-end')

    # logging
    print("Simulation finished. Emitted event 'simulation-end'")
