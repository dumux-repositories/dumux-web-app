import React from 'react'

const lineTypeToStyle = {
  'info': 'text-primary font-weight-bold',
  'result': 'text-light',
  'link': 'text-primary',
}

function TextOutputLine({ line, lineType, lineNumber }) {

  const classNames = 'output-line ' + lineTypeToStyle[lineType]

  // handle different line types
  switch (lineType) {
    case 'info':
      return (
        <div className={classNames}>
          <b>{line.text}</b>
        </div>
      )
    case 'result':
      return (
        <div className={classNames}>
          {line.text}
        </div>
      )
    case 'link':
      return (
        <div className={classNames}>
          <a href={line.link}>
            {line.text}
          </a>
        </div>
      )
    default:
      console.log("Unknown message type " + lineType)
      return null
  }
}

export default TextOutputLine
