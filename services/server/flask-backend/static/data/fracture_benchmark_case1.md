### Fracture Benchmark (Case 1): Single Fracture

This application features the first of the four benchmark cases presented in
[Berre et al (2020)](https://arxiv.org/abs/2002.07005), which were designed to compare
different numerical schemes for the simulation of flow and transport in fractured
porous media.

The setup is illustrated in the image below, where the fracture is depicted by
the gray surface, and the inflow and outflow boundaries are shown as blue and
purple regions, respectively. On the right side, the decomposition of the domain
is illustrated: the bulk porous medium is divided into two parts, of which the
upper part is further subdivided by the intersecting fracture.

For further details on the case setup and the mathematical model used to describe
flow and transport in fractured porous media, see
[Berre et al (2020)](https://arxiv.org/abs/2002.07005). In this application,
you can run the benchmark case with the numerical schemes available in
[Dumux](https://dumux.org/), which comprise two cell-centered (tpfa, mpfa)
and a vertex-centered finite volume scheme (box).
