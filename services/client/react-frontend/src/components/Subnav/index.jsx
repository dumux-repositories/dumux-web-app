import React from 'react'
import './subnav.css'

function Subnav({menuItems, onSelect, activeItem}) {

  let markup = []

  if(menuItems) {
    menuItems.forEach((menuItem, index) => {
      let extraClass = menuItem.value === activeItem ? 'active' : ''
      markup.push(
        <div className={'menu-item ' + extraClass} key={index} onClick={() => onSelect(menuItem.value)}>
          { menuItem.name }
        </div>
      )
    })
  }

  return (
    <div className='subnav-container'>
      <div className='subnav-container-inner'>
       { markup }
      </div>
    </div>
  )
}

export default Subnav
