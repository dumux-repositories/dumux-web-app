def registerExtensions(app, extensions):
    """
    Register all extensions with the app
    extensions (list): A list of extensions or tuple (extension, config)
    """
    for ext in extensions:
        if isinstance(ext, tuple):
            ext, config = ext # unpack keyword argument config
            print("Registering extension: {}".format(ext))
            print("Extension config: {}".format(config))
        else:
            config = {}
            print("Registering extension: {}".format(ext))

        ext.init_app(app, **config)
