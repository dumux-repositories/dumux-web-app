import React from 'react'
import './footer.css'

function Footer({reducedHeight}) {
  return <footer className={ reducedHeight ? 'reduced-height' : ''}>&copy; University of Stuttgart</footer>
}

export default Footer
