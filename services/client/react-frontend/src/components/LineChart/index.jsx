import React, { useRef, useEffect, useState, useMemo } from 'react'
import Chart from 'chart.js'
import Papa from 'papaparse'
import 'chartjs-plugin-colorschemes/src/plugins/plugin.colorschemes'
import './line-chart.css'
import themeColors from '../../theme/colors'
const numeral = require('numeral')

/*
 * Note: the chart js line chart has confusing naming conventions.
 * 'Labels' are the x-axis
 * 'Datasets' hold any y-axis data
 */
function LineChart({ fileContent, hidden, labels, datasets, fileURL }) {
  const chartCanvasRef = useRef(null)
  const [chart, setChart] = useState(null)

  // parse the csv text file as an array of objects
  const content = Papa.parse(fileContent, { header: true })

  // give each dataset an emply data property and style info
  if (datasets.length) {
    datasets.forEach((dataset, index) => {
      dataset.data = []
      dataset.borderColor = themeColors[index].hex
      dataset.backgroundColor = themeColors[index].rgba
      dataset.pointBorderColor = themeColors[index].rgba
      dataset.pointBackgroundColor = themeColors[index].hex
    })
  }

  // Give labels needs an empty data property
  labels.data = []

  function transformData(value, factor, offset) {
    // if there is a factor multiply by it
    if (factor) {
      value *= factor
    }
    // if there is an offset add it
    if (offset) {
      value += offset
    }

    return value
  }

  // loop through every row in the content
  content.data.forEach((row) => {
    // get the label value from this row
    let value = row[labels.key]
    if (value) {
      // push in to the data array after accounting for factor and offset
      labels.data.push(
        transformData(value, labels.factor, labels.offset)
      )
    }

    // loop through every dataset to extract values from the row
    datasets.forEach((dataset) => {
      let value = row[dataset.key]
      if (value) {
        dataset.data.push(
          transformData(
            value,
            dataset.factor,
            dataset.offset
          )
        )
      }
    })
  })

  // set some defaults
  Chart.defaults.global.animation = false
  Chart.defaults.global.defaultFontColor = '#ffffff'
  Chart.defaults.global.defaultFontSize = 16
  Chart.defaults.global.defaultFontFamily =
    '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"'

  // the chart configuration
  const chartConfig = useMemo(() => {
    return {
      type: 'line',
      data: {
        labels: labels.data,
        datasets: datasets,
      },
      options: {
        events: ['click'],
        legend: {
          display: true,
        },
        scales: {
          xAxes: [
            {
              display: true,
              gridLines: {
                color: 'rgba(255,255,255,0.1)',
              },
              scaleLabel: {
                display: labels.label ? true : false,
                labelString: labels.label,
              },
              ticks: {
                callback: (label) => {
                  return labels.format
                    ? numeral(label).format(labels.format)
                    : label
                },
              },
            },
          ],
          yAxes: [
            {
              display: true,
              gridLines: {
                color: 'rgba(255,255,255,0.1)',
              },
              scaleLabel: {
                display: false,
              },
            },
          ],
        },
      },
    }
  }, [labels.data, labels.format, labels.label, datasets])

  useEffect(() => {
    if (chart) {
      chart.data = chartConfig.data
      chart.options = chartConfig.options
      chart.update()
    } else {
      if (chartCanvasRef.current) {
        const context = chartCanvasRef.current.getContext('2d')
        setChart(new Chart(context, chartConfig))
      }
    }
  }, [chartConfig, labels.data, datasets, chart])

  useEffect(() => {
    return () => {
      chart && chart.destroy()
    }
  }, [chart])

  if (hidden) {
    return null
  }

  return (
    <div className='line-chart'>
      <a
        href={fileURL}
        className='btn-sm btn-primary float-right'
        target='_blank'
        rel='noopener noreferrer'
      >
        Show raw data
      </a>
      <canvas id='lineChart' ref={chartCanvasRef} />
    </div>
  )
}

export default LineChart
