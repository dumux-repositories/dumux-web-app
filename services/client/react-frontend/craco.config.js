const CracoVtkPlugin = require("craco-vtk")
const CracoItkPlugin = require("craco-itk")

module.exports = {
  plugins: [
    {
      plugin: CracoVtkPlugin()
    },
    {
      plugin: CracoItkPlugin()
    }
  ]
}
