FROM node:stretch as build
LABEL maintainer="timo.koch@iws.uni-stuttgart.de"

# create new working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# install node packages
COPY ./client/react-frontend/package.json /usr/src/app
COPY ./client/react-frontend/package-lock.json /usr/src/app
RUN npm install

# copy app content
COPY ./client/react-frontend /usr/src/app

# build the website
RUN npm run build

# in the second stage use build and configure nginx server
FROM nginx:alpine
COPY --from=build /usr/src/app/build /usr/share/nginx/html
RUN rm -f /etc/nginx/conf.d/*
COPY ./nginx/nginx.conf.template /etc/nginx/templates/flask-react-app.conf.template

ENV NGINX_HOST=0.0.0.0
ENV NGINX_PORT=80

EXPOSE 3000
