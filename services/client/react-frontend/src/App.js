import React from 'react'
import { Route, BrowserRouter as Router } from 'react-router-dom'
import SelectSimulation from './views/SelectSimulation'
import ConfigureSimulation from './views/ConfigureSimulation'
import RunSimulation from './views/RunSimulation'
import './App.css';

function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div>
        <Route exact path="/" component={SelectSimulation} />
        <Route path="/config/:id" component={ConfigureSimulation} />
        <Route path="/run/:id" component={RunSimulation} />
      </div>
    </Router>
  );
}

export default App;
