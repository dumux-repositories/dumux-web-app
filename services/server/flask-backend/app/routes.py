from flask import render_template, flash, request, session, redirect, url_for, json, jsonify
from flask import Blueprint
from flask_socketio import emit
from app.extensions import socketio
from app.simulation import docker as backend
import app.simulation.config as configtools

import traceback
import datetime
import url64
import os

routes = Blueprint('routes', __name__)

# Get available simulations
@routes.route("/api/simulations")
def getsimulations():
    # get a config file specifying available simulations
    simulationsConfig = os.environ.get('FLASK_APP_SIMULATIONS_CONFIG') or 'simulations.json'
    simulationsConfig = "static/data/" + simulationsConfig
    simulations = json.load(open(simulationsConfig))
    return jsonify(simulations)

# Get a test config file
@routes.route("/api/config")
def getconfig():
    simulationId = request.args.get('simulationId')
    config = json.load(open("static/data/" + simulationId + ".json"))
    configtools.checkConfig(config)
    config = configtools.applyTransformations(config)
    return jsonify(config)

def create_error_result(stderr):
    return {
        "computation": "some-id-dkanvasindoasindoaisdn",
        "status": "final",
        "timestamp": datetime.datetime.now().isoformat(),
        "notifications": {},
        "output": {"stdout": url64.encode(""),
                   "stderr": url64.encode(stderr)},
        "files": []
    }

# TODO is this a good way of reporting errors?
def abort(message):
    emit('simulation-result', create_error_result(message))
    emit('simulation-end')


# Decorator to catch an event called 'run-simulation'
@socketio.on('run-simulation')
def run_simulation(message):
    # get the config and
    # get the parameter template and render it
    # each web-app needs to specify a template parameter file in the
    # format that the simulation application understands
    # variables to be substituted are marked delimited by {{ }}
    try:
        # read backend configuration
        config = message["content"]["configuration"]

        # TODO Do we need to support multiple input files?
        input = config["inputfiles"][0]
        filename = input["filename"]
        if input["type"] != "template":
            raise ValueError("Only input type template is supported so far!")

        # open template parameter file
        with open("static/data/" + input["template"], 'r') as templateFile:
            template = templateFile.read()

        # render parameter file
        parameters = message["content"]["parameters"]
        for k, v in parameters.items():
            template = template.replace("{{{{ {} }}}}".format(k), str(v))

    except (KeyError, ValueError) as e:
        abort("Wrong simulation configuration: Abort.")
        traceback.print_exc()
        print("message: ", message)
        return

    # create message to run the simulation
    # create something that looks similar to the viplab messages
    message = {
        "type":"create-computation",
        "content":{
            "task":{
                "template":"cd39715e-55de-4563-bf8c-929d3d699953",
                "identifier":"11483f23-95bf-424a-98a5-ee5868c85c3e",
                "files": [
                    {
                        "identifier":"9ce43170-c5e2-4eb2-87b9-013d3836527f",
                        "parts": template,
                        "path": filename
                    }
                ]
            },
            "configuration": config
        }
    }

    # log message
    print("Received request to run a simulation.")

    # the backend runs the simulation
    backend.run_simulation(message)
