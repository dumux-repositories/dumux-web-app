import React from 'react'
import './loading-dots.css'

function LoadingDots() {
  return (
    <>
      <div className='loading-dots-animation'>
        <div className='dot'></div>
        <div className='dot'></div>
        <div className='dot'></div>
      </div>
    </>
  )
}

export default LoadingDots
